const { TeamsActivityHandler } = require("botbuilder");

const path = require("path");
const Choices = require("./service/Choices");

const ENV_FILE = path.join(__dirname, "..", ".env");

require("dotenv").config({ path: ENV_FILE });
class TeamsConversationBot extends TeamsActivityHandler {
  constructor() {
    super();

    this.help = "/h";

    let membersNumber = 0;

    this.onMembersAdded(async (context, next) => {
      const memberInfo = context.activity.membersAdded;

      membersNumber = memberInfo.length;

      console.log(membersNumber, "Members number");

      await next();
    });

    this.onMessage(async (context, next) => {
      console.log(membersNumber);

      const choice = new Choices(
        context,
        `${context.activity.text}`,
        membersNumber
      );

      choice.main();

      await next();
    });
  }
}

module.exports.TeamsConversationBot = TeamsConversationBot;
