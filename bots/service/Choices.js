const MessagesProcess = require("./MessagesProcess");

const commands = require("./../../JSON/config/cmd.json");

const Help = require("./../service/Help");
class Choices extends MessagesProcess {
  constructor(context, message, membersNumber) {
    super(context, message, membersNumber);

    this.context = context;
    this.message = message;
    this.membersNumber = membersNumber;
  }

  async main() {
    // Validação se o usuário enviou um comando válido.
    const validate = commands.cmd.filter(
      (cmdFind) => cmdFind == this.txtSistem
    );

    // Verificação do comando enviado pelo usuário.
    if (validate.length === 0) {
      try {
        await this.context.sendActivity(`${Help.sendHelpAll()}`);
      } catch (err) {
        console.log("err :" + err);
      }
    } else if (this.txtCommand === undefined) {
      try {
        await this.context.sendActivity(`${Help.sendSystemHelp(validate[0])}`);
      } catch (err) {
        console.log("erro :" + err);
      }
    }

    try {
      switch (this.txtSistem) {
        case "wso":
          await this.sendWso2();
          break;
        case "prints":
          await this.photoScreen();
          break;
        default:
          await this.systems();
          break;
      }
    } catch (err) {
      console.log("Erro ao executar envio choices:run" + err);
    }
  }
}

module.exports = Choices;
