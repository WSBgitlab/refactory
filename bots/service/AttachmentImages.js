const fs = require("fs");

const path = require("path");
class AttachmentImages {
  constructor() {
    this.pathname = path.resolve(__dirname, "..", "..", "graficos");
  }

  getInlineAttachment() {
    let imageData = fs.readFileSync(this.pathname + ".png");
    let base64Image = Buffer.from(imageData).toString("base64");

    return {
      name: "architecture-resize.png",
      contentType: "image/png",
      contentUrl: `data:image/png;base64,${base64Image}`,
    };
  }

  async handleOutgoingAttachment(turnContext) {
    try {
      let reply = { type: ActivityTypes.Message };

      reply.attachments = [this.getInlineAttachment()];

      await turnContext.sendActivity(reply);

      return true;
    } catch (err) {
      console.log("Erro ao anexat imagem " + err);

      return false;
    }
  }
}

module.exports = new AttachmentImages();
