
const util = require("util");

const exec = util.promisify(require("child_process").exec);

const { TurnContext } = require("botbuilder");

const SendMessages = require("../SendMessages");

class MessagesProcess {
 /**
  * 
  * @param {TurnContext} context 
  * @param {string} message 
  */
  constructor(context, message, membersNumber) {
    this.message = message;
    this.context = context;
    this.inCommand = 0;
    this.inSystem = 1;
    // Quantidade de membros, irá definir o index de comandos e sistemas
    
    /**
     * 
     * Até o momento teremos 3, sendo eles:
     * 
     * sistema -> será representado o sistema que deseja receber a mensagem
     * comando -> será a representação do gráfico dentro do grafana.
     * filas -> Alguns comandos terão um terceiro atributo para indicar a fila do determinado gráfico.
     * 
     */

    this.membersNumber = membersNumber;

    if (this.membersNumber >= 3){
      this.inSystem = 2;
      this.inCommand = 3;
    }

  }

  // sistemas

  async systems() {
    try {
      let message = this.message.split(" ");

      console.log(this.inCommand, 'index command');
      console.log(this.inSystem, 'index system');
      console.log(message, 'message');
      console.log(message[this.inSystem], 'sistema');
      console.log(message[this.inCommand], 'command');

      const result = await SendMessages.productSendMessage(
        this.context,
        message[this.inCommand],
        message[this.inSystem]
      );
      
      return result;
    } catch (error) {
      throw Error("Erro ao enviar mensagem, function system " + error);
    }
  }

  // PhotoScreen com um único comando

  async photoScreen() {
    try {
      let message = this.message.split(" ");

      const result = await SendMessages.sendScreen(
        this.context,
        message[this.inSystem],
        message[this.inCommand]
      );

      return result;
    } catch (error) {
      throw Error(
        "Erro ao processar print screen, function photoScreen" + error
      );
    }
  }

  // Wso
  async sendWso2() {
    let comando = `xvfb-run --server-args="-screen 0,1024x768x24" wkhtmltoimage --window-status imdone --run-script 'window.setTimeout(function (){window.status = "imdone";},6000);' > ${path.resolve(
      __dirname,
      "..",
      "graphics",
      "wso.png"
    )}`;

    try {
      await exec(comando);

      await AttachmentImages.handleOutgoingAttachment(this.context);

      return true;
    } catch (err) {
      console.log("Ocorreu erro ao enviar o gráfico wso2", err);
      return false;
    }
  }

  // Ajuda geral
  
  async help(){
    try {
      const result = await SendMessages.sendHelp();

      return result;
    } catch (error) {
      console.log("Erro ao enviar ajuda geral fun:help" + error)
    }
  }

  get sizeParams(){
    return this.message.length;
  }

  get txtSistem(){
    let message = this.message.split(" ");
    
    console.log(this.inSystem, "sistema");
    console.log(message, "222");

    return message[this.inSystem];
  }

  get txtCommand(){
    let message = this.message.split(" ");

    return message[this.inCommand];
  }
}

module.exports = MessagesProcess;