const puppeteer = require("puppeteer");

const path = require("path")

module.exports = {
  async init(url,name){
    const browser = await puppeteer.launch({
      defaultViewport: {width: 1920, height: 1080},
      args: ['--no-sandbox','--ignore-certificate-errors','--use-gl']
    })

    const page = await browser.newPage();

    await page.setRequestInterception(true);

    page.on('request', async (request) => {
      request.continue({
        headers: {
          'Authorization': `Bearer ${process.env.TOKEN_GRAFANA}`,
          'Referer': `${url}`,
          'Content-Type': 'application/json',
          'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'
        }
      })
    })
    
    await page.setCookie(
      {
        name : 'grafana_remember',
        url: `${url}`,
        value : '4796d48d0822ba786f8838b445dcfbb6fe65a3a99a7d84c9a6fe36690bc5ebc8594971c5028c9632dbf4aa49286a2a64ff65e5f7365e0c'    
      },
      {
        name : 'grafana_sess',
        url: `${url}`,
        value : 'c73f2d21c501b35d'
            
      },
      {
        name : 'grafana_user',
        url: `${url}`,
        value : 'Wellington+da+Silva+Bezerra'     
      }
    );

    await page.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36");
    
    await page.waitFor(8000);

    await page.goto(`${url}`);

    await page.waitFor(8000);

    await page.screenshot({ path : path.resolve('graphics',`${name}.png`)});
    
    await page.waitFor(2000);

    await browser.close();
  }
}
