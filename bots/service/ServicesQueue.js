
class ServiceQueue{
    async sendSlaCentral(context, command, queue) {
        let urls = [
          `https://dashboard.cip-core.local:8443/render/d-solo/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&panelId=11&var-Queue=${queue.toUpperCase()}&width=1000&height=500&tz=America%2FSao_Paulo`,
          `https://dashboard.cip-core.local:8443/render/d-solo/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&panelId=12&var-Queue=${queue.toUpperCase()}&width=1000&height=500&tz=America%2FSao_Paulo`,
          `https://dashboard.cip-core.local:8443/render/d-solo/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&panelId=13&var-Queue=${queue.toUpperCase()}&width=1000&height=500&tz=America%2FSao_Paulo`,
          `https://dashboard.cip-core.local:8443/render/d-solo/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&panelId=6&var-Queue=${queue.toUpperCase()}&width=1000&height=500&tz=America%2FSao_Paulo`,
          `https://dashboard.cip-core.local:8443/render/d-solo/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&panelId=7&var-Queue=${queue.toUpperCase()}&width=1000&height=500&tz=America%2FSao_Paulo`,
          `https://dashboard.cip-core.local:8443/render/d-solo/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&panelId=5&var-Queue=${queue.toUpperCase()}&width=1000&height=500&tz=America%2FSao_Paulo`,
          `https://dashboard.cip-core.local:8443/render/d-solo/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&panelId=10&var-Queue=${queue.toUpperCase()}&width=1000&height=500&tz=America%2FSao_Paulo`,
          `https://dashboard.cip-core.local:8443/render/d-solo/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&panelId=4&var-Queue=${queue.toUpperCase()}&width=1000&height=500&tz=America%2FSao_Paulo`,
          `https://dashboard.cip-core.local:8443/render/d-solo/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&panelId=14&var-Queue=${queue.toUpperCase()}&width=1000&height=500&tz=America%2FSao_Paulo`,
          `https://dashboard.cip-core.local:8443/render/d-solo/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&panelId=2&var-Queue=${queue.toUpperCase()}&width=1000&height=500&tz=America%2FSao_Paulo`,
        ];
    
    
        /**
         *  Testando novo método para envio de mensagens
         */
    
        // New
        urls.forEach((url, index) => {
            try{
                let { stdout } = await exec(`
                    /bin/curl "${url}" -H "Authorization: Bearer ${process.env.TOKEN_GRAFANA}" --compressed --insecure > /opt/bot-cip/bot-teams-cip/graphics/${command}_${queue}${index}.png
                `);
    
    
                console.log("Enviando : " + command + " - " + `${queue}${index}`);
    
                // Instanciar a classe nova service/AttachmentImages
                await AttachmentImages.handleOutgoingAttachment(context);
            }catch(err){
                throw Error("Erro ao fazer o downloading das imagens!" + err);
            }
        })
    // Old
    /**
     * 
    for (let i = 0; i < urls.length; i++) {
      
        try{
            let { stdout } = await exec(`
                /bin/curl "${urls[i]}" -H "Authorization: Bearer ${process.env.TOKEN_GRAFANA}" --compressed --insecure > /opt/bot-cip/bot-teams-cip/graphics/${command}_${queue}${i}.png
            `);
        }catch(err){
            throw Error("Erro ao fazer o downloading das imagens!" + err);
        }
    }

    for (let i = 0; i < urls.length; i++) {
        try {
            await this.handleOutgoingAttachment(
              context,
              path.resolve(__dirname, "..", "graphics", `${command}_${queue}${i + 1}`)
            );
        } catch (error) {
            throw Error("Erro ao realizar o envio das imagens!" + err)
        }
    }
    
     */

    return true
  }
}