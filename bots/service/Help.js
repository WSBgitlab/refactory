const path = require("path");

const dataHelp = require("./../../JSON/help/help.json");
class MessagesHelp {
  sendSystemHelp(command) {
    try {
      const systemHelp = require(path.resolve(
        __dirname,
        "..",
        "..",
        "JSON",
        "sistemas",
        `cip_${command}.json`
      ));


      console.log(systemHelp);

      // Ordena o comando por ordem alfabética.
      let textOrdenado = this.sortWithCmd(systemHelp.data);

      return textOrdenado;
    } catch (erro) {
      throw Error("Erro ao enviar mensagem de help! (SISTEMA)" + erro);
    }
  }

  sendHelpAll() {
    let text = "<br><br> <b>Bot Teams Grafana</b> <br><br>";

    text += `Olá seja bem vindo ao help do bot grafana! <br> @grafana **sistema** **comando**<br>`;

    text += `<br>Exemplo de uso: @grafana pcr /h - Ajuda do sistema PCR<br>`;

    text += `<br>Utilize /h para solicitar ajuda dos sistemas<br>`;

    text += `<br>Lista de sistemas disponíveis<br>`;

    let ordena = dataHelp.help.sort(function (str, strcompare) {
      if (str.command > strcompare.command) {
        return 1;
      }
      if (str.command < strcompare.command) {
        return -1;
      }

      return 0;
    });

    ordena.map((data) => (text += `<b> - ${data.command} </b><br>`));

    return text;
  }

  sortWithCmd(array) {
    let text = "";

    let ordena = array.sort(function (str, strcompare) {
      if (str.cmd > strcompare.cmd) {
        return 1;
      }
      if (str.cmd < strcompare.cmd) {
        return -1;
      }

      return 0;
    });

    ordena.map(
      (data) => (text += `<b> - ${data.cmd}</b> ${data.help}<br>`)
    );

    return text;
  }
}

module.exports = new MessagesHelp();
