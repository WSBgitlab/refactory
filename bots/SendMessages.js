const { MessageFactory, TurnContext } = require("botbuilder");

const path = require("path");

const util = require("util");

const exec = util.promisify(require("child_process").exec);

const fs = require("fs");

const AttachmentImages = require("./service/AttachmentImages");

const MessagesHelp = require("./service/Help");

const screen = require("./service/Puppeteer");

class SendMessage {
  constructor() {
    this.help = "/h";
  }

  /**
   *
   * @param {TurnContext} context
   * @param {string} system
   * @param {string} command
   *
   * Será realizado o download no grafana e enviado ao usuário solicitado.
   */
  async productSendMessage(context, command, system) {
    try {
      console.log("sistema", system);
      console.log("command", command);

      const readJson = require(path.resolve(
        __dirname,
        "..",
        "JSON",
        "sistemas",
        `cip_${command}.json`
      ));

      console.log(command == null);
      console.log(command == this.help);
      console.log(this.help === '/h');

      if (command == null || command === this.help) {
        await context.sendActivity(`${MessagesHelp.sendSystemHelp(command)}`);
      }

      const findCommand = readJson.data.filter((item) => item.cmd === command);

      if (findCommand.length == 0)
        await context.sendActivity(
          `<br> Comando inválido <br>${MessagesHelp.sendSystemHelp(command)}`
        );

      await context.sendActivity(
        `Aguarde um momento, esse processo pode demorar até 1 minuto.`
      );

      const resultSendGraphic = await this.sendGraphic(
        context,
        findCommand.url,
        system,
        command
      );

      return resultSendGraphic;
    } catch (err) {
      throw Error("Erro enviar mensagem productSendMessage" + err);
    }
  }

  /**
   *
   * @param {TurnContext} context
   * @param {string} system
   * @param {string} command
   */
  async sendScreen(context, system, command) {
    const dashs = require(path.resolve("..", "JSON", "cip_prints.json"));

    if (command === `${this.help}` || command === null)
      await context.sendActivity(`${MessagesHelp.sendSystemHelp(command)}`);

    let findCommand = dashs.data.filter((item) => item.cmd === system);

    if (findCommand.length === 0)
      await context.sendActivity(`Esse comando não foi encontrado!`);

    await context.sendActivity(
      `Aguarde um momento, esse processo pode demorar até 1 minuto.`
    );

    await screen.init(`${findCommand[0].url}`, `${findCommand[0].cmd}`);

    try {
      await this.sendScreenGrafana(context, `${findCommand[0].cmd}`);
    } catch (err) {
      console.log("erro ao enviar printscreen!" + err);
    }
  }

  async notFound(context) {
    await context.sendActivity(
      MessageFactory.text(
        `Comando não encontrado com sucesso! ${MessagesHelp.sendHelpAll()}`
      )
    );
  }

  /**
   *
   * @param {TurnContext} context
   * @param {string} command
   */
  async sendScreenGrafana(context, command) {
    try {
      await AttachmentImages.handleOutgoingAttachment(
        context,
        path.resolve(__dirname, "..", "graphics", `${command}`)
      );
    } catch (err) {
      console.log("Erro ao enviar a imagem pelo printscreen", err);
    }
  }

  /**
   *
   * @param {TurnContext} context
   * @param {string} url
   * @param {string} command
   * @param {string} graphic
   */
  async sendGraphic(context, url, command, graphic) {
    await exec(
      `/bin/curl "${url}" -H "Authorization: Bearer ${process.env.TOKEN_GRAFANA}" --compressed --insecure > /opt/bot-cip/bot-teams-cip/graphics/${command}_${graphic}.png`
    );

    try {
      const sendImage = await AttachmentImages.handleOutgoingAttachment(
        context,
        path.resolve(__dirname, "..", "graphics", `${command}_${graphic}`)
      );

      return sendImage;
    } catch (err) {
      console.log("erro ao enviar imagem para o teams", err);
    }
  }

  /**
   *
   * @param {TurnContext} context
   * @param {string} command
   * @param {string} graphic
   */
  async sendDashboard(context, command, graphic) {
    // Ler arquivo que contém as urls
    let file = fs.readFileSync(
      path.resolve(__dirname, `graphics`, `${command}_${graphic}.txt`)
    );

    // Filtro para validar se o arquivo existe dentro de um arquivo de controle.
    let { stdout } = await exec(`
      cat ${path.resolve(__dirname)}/graphics/${command}_${graphic}.txt | wc -l
    `);

    // Quantidade de linhas que o arquivo encontrado contém.
    let qtdRow = parseInt(stdout);

    // Executar comando para realizar o download dos gráficos.
    const outputDownload = await exec(`
      cat ${path.resolve(
        __dirname
      )}/graphics/${command}_${graphic}.txt | while read x y z
        do
          echo $y
          curl "$x" -H "Authorization: Bearer ${
            process.env.TOKEN_GRAFANA
          }" --compressed --insecure > /opt/bot-cip/bot-teams-cip/graphics/${command}_"$z".png
          echo "============"
        done
    `);

    // envio para o bot
    for (let i = 0; i < qtdRow; ++i) {
      await this.handleOutgoingAttachment(
        context,
        path.resolve(
          __dirname,
          "..",
          "graphics",
          `${command}_${graphic}${i + 1}`
        )
      );
    }

    return true;
  }

  async sendHelp(context) {
    try {
      await context.sendActivity(
        MessageFactory.text(`${MessagesHelp.sendHelpAll()}`)
      );
    } catch (err) {
      console.log("Erro ao enviar help sendhelp:sendmessages" + err);
    }
  }
}

module.exports = new SendMessage();
