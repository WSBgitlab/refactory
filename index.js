const restify = require("restify");

const fs = require("fs");

const path = require("path");

const { BotFrameworkAdapter } = require("botbuilder");

const { TeamsConversationBot } = require("./bots/teamsConversationBot");

const appId = "";

const appPassword = "";

const adapter = new BotFrameworkAdapter({
  appId: appId,
  appPassword: appPassword,
});

const bot = new TeamsConversationBot();

const options = {
  certificate : fs.readFileSync(path.resolve(__dirname,"portalcrisecip.cip-bancos.org.br.pem")),
  key : fs.readFileSync(path.resolve(__dirname,"portalcrisecip.cip-bancos.org.br.key"))
}

const server = restify.createServer();

server.get('/graphics/*', restify.plugins.serveStatic({
  directory : __dirname
}));

// Listen for incoming requests.
server.post("/api/messages", (req, res) => {
  adapter.processActivity(req, res, async (context) => {
    await bot.run(context);
  });
});

server.listen(process.env.port || process.env.PORT || 3980,"localhost", function () {
  console.log(`\n${server.name} listening to ${server.url}`);
});
