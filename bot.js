const { ActivityHandler, MessageFactory, CardFactory, ActivityTypes, TeamsInfo, TurnContext } = require('botbuilder');
const axios = require('axios');
const moment = require('moment');
const fs = require('fs');

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const CONVERSATION_DATA_PROPERTY = 'conversationDataProperty';
const USER_PROFILE_PROPERTY = 'userProfileProperty';
const ADDRESS_BACKEND_CHATBOT = "https://172.26.6.33:3040";

class EchoBot extends ActivityHandler {
    constructor(conversationState, userState, conversationReferences) {
        super();

        this.conversationReferences = conversationReferences;

        this.onConversationUpdate(async (context, next) => {
            this.addConversationReference(context);
            await next();
        });

        this.conversationState = conversationState;
        this.conversationDataAccessor = this.conversationState.createProperty(CONVERSATION_DATA_PROPERTY);
        this.userState = userState;
        this.userProfileAccessor = this.userState.createProperty(USER_PROFILE_PROPERTY);

        let requestPayload = {};

        this.onMessage(async (context, next) => {
            this.addConversationReference(context);

            const userProfile = await this.userProfileAccessor.get(context, {});
            const conversationData = await this.conversationDataAccessor.get(context, { botContext: '' });

            if (!userProfile.loggedUser && !userProfile.userName) {
                const member = await TeamsInfo.getMembers(context);
                let requestBody = { email: member[0].email, fullname: member[0].name };

                const response = await axios.post(`${ADDRESS_BACKEND_CHATBOT}/check-user-id`, requestBody);
                userProfile.loggedUser = response.data.idUsuario.toUpperCase();
                userProfile.userName = member[0].givenName;
            }

            const userInput = context.activity.text;
            let checagem = userInput.match(/(\s?menu principal\s?|novo atendimento|\s?oi bart\s?|\s?olá bart\s?|\s?ola bart\s?)/gi);

            if ((checagem !== null) || (userInput.toUpperCase() === "OI")) {
                requestPayload = {
                    context: {
                        domain: "",
                        loggedUser: userProfile.loggedUser,
                        userName: userProfile.userName
                    }
                };
            } else {
                requestPayload = {
                    "input": {
                        "text": userInput
                    },
                    "context": conversationData.botContext
                };
            }

            if (requestPayload.context !== '') {
                try {
                    const response = await axios.post(`${ADDRESS_BACKEND_CHATBOT}/api/message`, requestPayload);

                    let resposta = response.data.output.generic;
                    conversationData.botContext = response.data.context;

                    await context.sendActivities([
                        { type: ActivityTypes.Typing }
                    ]);

                    let filtroText = resposta.filter(retorno => retorno.response_type === 'text');
                    for (let i = 0; i < filtroText.length; i++) {
                        let retorno = filtroText[i];
                        await context.sendActivity(MessageFactory.text(retorno.text, retorno.text));
                    }

                    let filtroOutros = resposta.filter(retorno => retorno.response_type !== 'text');
                    for (let i = 0; i < filtroOutros.length; i++) {
                        let retorno = filtroOutros[i];

                        if(retorno.options.length > 30){
                            let qtdePartes = Math.floor(retorno.options.length/30);
                            let pacoteOpcoes = [];

                            for(i=0; i <= qtdePartes; i++){
                                let inicio = i*30;
                                let final = (i+1)*30;
                                pacoteOpcoes.push(retorno.options.slice(inicio, final));
                            }

                            let activities = [];
                            activities.push({ type: "message", text: retorno.title });

                            pacoteOpcoes.forEach(pacote => {
                                
                                pacote = pacote.map(option => {
                                    return {
                                        type: 'messageBack',
                                        title: option.label,
                                        text: option.value.input.text,
                                        displayText: option.value.input.text
                                    }
                                });

                                activities.push(MessageFactory.attachment(
                                    CardFactory.heroCard(null, undefined, pacote, "Escolha um evento:")
                                ));
                            });

                            await context.sendActivities(activities);
                        }else{
                            const options = retorno.options.map(option => {
                                let checkVoltar = option.label.match(/<i class='material-icons btn-voltar'>navigate_before<\/i> /gi);
                                if (checkVoltar !== null) {
                                    option.label = option.label.replace("<i class='material-icons btn-voltar'>navigate_before</i> ", "");
                                }
    
                                return {
                                    type: 'messageBack',
                                    title: option.label,
                                    text: option.value.input.text,
                                    displayText: option.value.input.text
                                }
                            });
    
                            if (retorno.response_type === "mixed-options") {
                                options.push({
                                    type: 'messageBack',
                                    title: "Voltar ao menu anterior",
                                    text: "Voltar ao menu anterior",
                                    displayText: "Voltar ao menu anterior"
                                })
                            }
    
                            const message = MessageFactory.attachment(
                                CardFactory.heroCard(null, undefined, options, { text: retorno.title })
                            );
    
                            await context.sendActivity(message);
                        }
                    }

                    let agora = moment().add(15, "m");
                    userProfile.timeout = agora.format("DD/MM/YYYY HH:mm");
                    userProfile.nextNode = response.data.context.nextNode;
                } catch (error) {
                    let errorLog = JSON.stringify(error) + "\n------------------------------\n\n";
                    fs.appendFileSync("./logs.txt", errorLog);
                }
            } else {
                await context.sendActivities([{ type: ActivityTypes.Typing }]);
                let errorMessage = "Por favor, inicie um novo atendimento utilizando uma das frases: <strong>oi</strong>, <strong>oi bart</strong>, <strong>olá bart</strong>, <strong>menu principal</strong> ou <strong>novo atendimento</strong>";
                await context.sendActivity(MessageFactory.text(errorMessage, errorMessage));
            }

            await next();
        });
    }

    async run(context) {
        await super.run(context);
        await this.conversationState.saveChanges(context, false);
        await this.userState.saveChanges(context, false);
    }

    async addConversationReference(context){
        const conversationReference = TurnContext.getConversationReference(context.activity);
        this.conversationReferences[conversationReference.conversation.id] = conversationReference;
        
        const member = await TeamsInfo.getMembers(context);
        fs.writeFileSync(`./activity_references/${member[0].email}.json`, JSON.stringify(conversationReference));
    }
}

module.exports.EchoBot = EchoBot;
