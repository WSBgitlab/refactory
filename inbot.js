const { BotFrameworkAdapter, ConversationState, UserState, MemoryStorage, MessageFactory, CardFactory } = require('botbuilder');
const { MicrosoftAppCredentials } = require('botframework-connector');
const { EchoBot } = require('./bot');

const path = require('path');
const dotenv = require('dotenv');
const ENV_FILE = path.join(__dirname, '.env');
const restify = require('restify');
const fs = require('fs');
const moment = require("moment");
const axios = require('axios');

const memoryStorage = new MemoryStorage();
const conversationState = new ConversationState(memoryStorage);
const userState = new UserState(memoryStorage);
const conversationReferences = {};

const ADDRESS_BACKEND_CHATBOT = "https://172.26.6.33:3040";
const DOMINIO_SERVICEPORTAL = "portaldeservicos.cip-bancos.org.br";
var statusEvento = "EMVALID";

dotenv.config({ path: ENV_FILE });

const options = {
    certificate: fs.readFileSync(path.resolve(__dirname, "portalcrisecip.cip-bancos.org.br.pem")),
    key: fs.readFileSync(path.resolve(__dirname, "portalcrisecip.cip-bancos.org.br.key"))
}

const server = restify.createServer(options);

server.listen(process.env.port || process.env.PORT || 3979, async () => {
    console.log(`\n${server.name} listening to ${server.url}`);
    setInterval(checkEventoStatus, 600000); //10min
    setInterval(checkTimeout, 60000); //01min
});

const checkEventoStatus = async () => {
    statusEvento = statusEvento === "PENDUSR" ? "EMVALID": "PENDUSR";
    let rangeInicio = moment().subtract(20, "minutes").format();
    let rangeFim = moment().format();

    let log = {};
    log.tipo = statusEvento;

    log.inicioExecucao = moment();

    let requestBody = {
        status: statusEvento,
        dataInicio: rangeInicio,
        dataFim: rangeFim
    };

    const response = await axios.post(`${ADDRESS_BACKEND_CHATBOT}/consulta-status-evento`, requestBody);

    log.usuarios = [];
    log.usuariosNaoEncontrados = [];

    log.qtdeEventosEnviados = 0;
    log.qtdeEventosTotal = 0;

    if(response.data.status === undefined){
        let eventos = response.data;

        Object.keys(eventos).forEach(usuario => {
            eventos[usuario] = eventos[usuario].filter(evento => {
                let dataModificacaoEvento = moment(evento.CHANGEDATE);
                let dataCorte = moment("2020-12-15");

                if(dataModificacaoEvento.isSameOrAfter(dataCorte)){
                    return evento;
                }
            });
        });
        
        let usuarios = Object.keys(eventos);
        log.usuarios = usuarios;

        if(usuarios.length > 0){
            await usuarios.forEach(async usuario => {

                let contextoUsuario = getUserReference(usuario);

                if(contextoUsuario){
                    await adapter.continueConversation(contextoUsuario, async context => {                    
                        MicrosoftAppCredentials.trustServiceUrl(contextoUsuario.serviceUrl);
                        
                        let activities = [];
                        let eventosUsuario = eventos[usuario];
                        log.qtdeEventosTotal += eventosUsuario.length;

                        if(statusEvento === "PENDUSR"){
                            if(eventosUsuario.length > 1){
                                activities.push({ type: "message", text: "Os seguintes eventos foram devolvidos por falta de informações" });
                            }else{
                                activities.push({ type: "message", text: "O seguinte evento foi devolvido por falta de informações" });
                            }
                        }else{
                            if(eventosUsuario.length > 1){
                                activities.push({ type: "message", text: "Os seguintes eventos foram atendidos e aguardam sua avaliação" });
                            }else{
                                activities.push({ type: "message", text: "O seguinte evento foi atendido e aguarda sua avaliação" });
                            }
                        }

                        eventosUsuario.forEach(async evento => {
                            let proactiveMessage = "";

                            if(statusEvento === "PENDUSR"){
                                proactiveMessage += `<strong>EVENTO:</strong> ${evento.TICKETID}<br>`;
                                proactiveMessage += `<strong>SISTEMA:</strong> ${evento.CINUM}<br>`;

                                let detalhes = "-";

                                if(evento.DESCRIPTION_LONGDESCRIPTION != undefined){
                                    detalhes = evento.DESCRIPTION_LONGDESCRIPTION;
                                    
                                    if(evento.DESCRIPTION_LONGDESCRIPTION.length > 200){
                                        detalhes = `${evento.DESCRIPTION_LONGDESCRIPTION.substring(0,200)}...`;
                                    }
                                }
                                
                                proactiveMessage += `<strong>DETALHES:</strong> ${detalhes}<br>`;
                                
                                let ultimoLog = "-";

                                if(evento.WORKLOG_DESCRIPTION_LONGDESCRIPTION != undefined){
                                    ultimoLog = evento.WORKLOG_DESCRIPTION_LONGDESCRIPTION;
                                    
                                    if(evento.WORKLOG_DESCRIPTION_LONGDESCRIPTION.length > 200){
                                        ultimoLog = `${evento.WORKLOG_DESCRIPTION_LONGDESCRIPTION.substring(0,200)}...`;
                                    }
                                }

                                proactiveMessage += `<strong>ÚLTIMO LOG:</strong> ${ultimoLog}<br>`;

                                proactiveMessage += "<br>";
                                proactiveMessage += `Para verificar mais detalhes, <a target="_blank" href="https://${DOMINIO_SERVICEPORTAL}/portal/default/self-service/#/issues/${evento.TICKETUID}">CLIQUE AQUI</a>`;
                            }else{
                                proactiveMessage += `<strong>EVENTO:</strong> ${evento.TICKETID}<br>`;
                                proactiveMessage += `<strong>STATUS:</strong> Em validação<br>`;
                                proactiveMessage += `<strong>RESUMO:</strong> ${evento.DESCRIPTION}<br>`;

                                let resposta = "-";

                                if(evento.CIPRESPOSTA != undefined){
                                    resposta = evento.CIPRESPOSTA;

                                    if(evento.CIPRESPOSTA.length > 200){
                                        resposta = `${evento.CIPRESPOSTA.substring(0,200)}...`;
                                    }
                                }

                                proactiveMessage += `<strong>RESPOSTA AO SOLICITANTE:</strong> ${resposta}<br>`;

                                proactiveMessage += "<br>";
                                proactiveMessage += `Para verificar mais detalhes, <a target="_blank" href="https://${DOMINIO_SERVICEPORTAL}/portal/default/self-service/#/issues/${evento.TICKETUID}">CLIQUE AQUI</a>`;
                            }

                            log.qtdeEventosEnviados++;
                            activities.push({ type: "message", text: proactiveMessage });
                        });

                        await context.sendActivities(activities);
                    });
                }else{
                    log.usuariosNaoEncontrados.push(usuario);
                }
            });
        }
    }

    log.fimExecucao = moment();

    let txtLog = preencheTemplate(log);
    fs.appendFileSync(`./logs/execucao-varredura-eventos.txt`, txtLog);
}

const preencheTemplate = log => {
    let txtLog = '';
    
    txtLog += `************ INÍCIO EXECUÇÃO CHECK EVENTOS ************`;
	txtLog += `\nTipo: ${log.tipo}`;
	txtLog += `\nInício: ${log.inicioExecucao}`;
	txtLog += `\nFim: ${log.fimExecucao}`;	

	txtLog += `\n\n******* EVENTOS ENCONTRADOS *******`;
	txtLog += `\nQuantidade de eventos encontrados: ${log.qtdeEventosTotal}`;	
    txtLog += `\nQuantidade de eventos notificados com sucesso: ${log.qtdeEventosEnviados}`;	
    
    if(log.usuarios.length > 0){
        txtLog += `\n\n********** USUÁRIOS ENCONTRADOS **********`;
        txtLog += `\n${JSON.stringify(log.usuarios)}`;
    }

    if(log.usuariosNaoEncontrados.length > 0){
        txtLog += `\n\n********* USUÁRIOS NÃO ENCONTRADOS NO TEAMS *********`;
        txtLog += `\n${JSON.stringify(log.usuariosNaoEncontrados)}`;
    }
    
    txtLog += `\n\n********** FIM EXECUÇÃO CHECK EVENTOS **********\n\n`;
	return txtLog;
}

const getUserReference = email => {
    if(fs.existsSync(`./activity_references/${email}.json`)){
        let registro = fs.readFileSync(`./activity_references/${email}.json`);
        contentUser = JSON.parse(registro);
        return contentUser;
    }

    return false;
}

const checkTimeout = async () => {
    let chaves = Object.keys(conversationState.storage.memory);
    let registros = conversationState.storage.memory;

    if (chaves.length > 0) {
        let filtro = chaves.map(chave => chave.match(/msteams\/users\/(.*)\//ig))
            .filter(chave => chave !== null)
            .map(chave => chave[0]);

        let filtroConversas = chaves.map(chave => chave.match(/msteams\/conversations\/(.*)\//ig))
            .filter(chave => chave !== null)
            .map(chave => chave[0]);

        filtro.forEach(async registro => {
            if(registros[registro] !== undefined){
                let infoUsuario = JSON.parse(registros[registro]);
                
                if (Object.keys(infoUsuario).length > 0) {
                    if (infoUsuario.userProfileProperty.userName) {
                        let agora = moment().format("DD/MM/YYYY HH:mm");
    
                        if (agora === infoUsuario.userProfileProperty.timeout && infoUsuario.userProfileProperty.nextNode !== "NOVO_ATENDIMENTO") {
                            let userID = registro.match(/(?<=msteams\/users\/)(.*)(?=\/)/gi)[0];
                            let conversationReference = Object.values(conversationReferences).filter(reference => reference.user.id === userID)[0];
                            await adapter.continueConversation(conversationReference, async context => {
                                
                                let options = [
                                    {
                                        type: 'messageBack',
                                        title: "Novo atendimento",
                                        text: "Novo atendimento",
                                        displayText: "Novo atendimento"
                                    }
                                ];
    
                                let timeoutText = "Ops, este atendimento foi encerrado por inatividade. Caso queira iniciar um novo atendimento, clique no botão abaixo:";
    
                                const message = MessageFactory.attachment(
                                    CardFactory.heroCard(null, undefined, options, { text: timeoutText })
                                );
    
                                filtroConversas.forEach(async conversas => {
                                    let infoConversa = JSON.parse(registros[conversas]);
                                    let contextoUsuario = null;
                                    
                                    if(infoUsuario.userProfileProperty.loggedUser === infoConversa.conversationDataProperty.botContext.loggedUser){
                                        contextoUsuario = infoConversa.conversationDataProperty.botContext;
                                        contextoUsuario.nextNode = "NOVO_ATENDIMENTO";
    
                                        requestPayload = {
                                            "input": {
                                                "text": "TIMEOUT_ENCERRA"
                                            },
                                            "context": contextoUsuario
                                        };
    
                                        await axios.post(`${ADDRESS_BACKEND_CHATBOT}/api/message`, requestPayload);
                                    }
                                });
    
                                await context.sendActivity(message);
                                await userState.delete(context);
                                await conversationState.delete(context);
                            });
                        }
                    }
                }
            }
        });
    }
}

const adapter = new BotFrameworkAdapter({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword
});

const onTurnErrorHandler = async (context, error) => {
    console.error(`\n [onTurnError] unhandled error: ${error}`);
    await context.sendTraceActivity(
        'OnTurnError Trace',
        `${error}`,
        'https://www.botframework.com/schemas/error',
        'TurnError'
    );
    await context.sendActivity('Não consegui entender sua ação.');
    await context.sendActivity('Por favor, digite novamente.');
};

adapter.onTurnError = onTurnErrorHandler;
const myBot = new EchoBot(conversationState, userState, conversationReferences);

server.post('/api/messages', (req, res) => {
    adapter.processActivity(req, res, async (context) => {
        await myBot.run(context);
    });
});

server.on('upgrade', (req, socket, head) => {
    const streamingAdapter = new BotFrameworkAdapter({
        appId: process.env.MicrosoftAppId,
        appPassword: process.env.MicrosoftAppPassword
    });

    streamingAdapter.onTurnError = onTurnErrorHandler;
    streamingAdapter.useWebSocket(req, socket, head, async (context) => {
        await myBot.run(context);
    });
});
